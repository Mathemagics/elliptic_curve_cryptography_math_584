# Copyright (c) 2020 Adrian Vazquez <vazquez.scholar@gmail.com>

# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


import matplotlib.pyplot as plt
import numpy as np

#Point = namedtuple("Point", ['x','y'])

#prime number for finite field goes here
p:int = 19
#Prime number for elliptic curve goes here
A:int = -3
B:int = 17

#plotting limits
x_min = 0
x_max = p

#precompute inverses for cheap lookups
inverses = {i:j for i in range(0,p) for j in range(0,p) if ((i*j)%p==1)}    

# General cubic        
f = lambda x: x**3+A*x+B

class Point:
    def __init__(self, x:int, y:int):
        self.x = x
        self.y = y
    
    def get_slope(self, b):
    #Special Case
        if self.x == b.x:
            #print("Special Case!")
            #Super Special Case!
            if self.check_infinite(b):
                #print("Super special case!")
                return np.Inf
            else:
                rise = ((3*self.x**2)+A) % p
                run = inverses[(2*self.y) % p]
                return (rise*run) % p
        else:
            rise = (self.y-b.y) % p
            run = (self.x-b.x) % p
            slope = (rise*inverses[run]) % p
            return slope

    def check_infinite(self, b):
        if (self.y != b.y):
            return True
        elif (self.y == 0 and b.y==0):
            return True
        else: 
            return False
    
    def __add__(self, b):
        if(self.isnan()): return self
        
        m = (self.get_slope(b)) % p
        x_3 = ((m**2)%p - (self.x)%p - (b.x)%p) % p
        y_3 = -(self.y + m*(x_3-self.x))%p
        return Point(x_3,y_3)       
    
    # def __eq__(self, b):
    #     return (self.x == b.x) and (self.y == b.y)
    
    def __str__(self):
        return f"Point(x={self.x},y={self.y})"
    
    def __repr__(self):
        return self.__str__()
    
    def isnan(self):
        return np.isnan(self.x) or np.isnan(self.y)
    
    def scale(self, n:int):
        b=Point(self.x, self.y)
        for i in range(1,n+1):
            if(b.isnan()):
                return b
            else: 
                b=b+self
        return b
    
    def get_cycle(self):
        b=Point(self.x, self.y)
        i = 0
        while not b.isnan():
            b=b+self
            i=i+1
        return i

def get_squared_ys():
    vals={}
    for y in range(0,p):
        y_sqr = y**2 %p
        if y_sqr in vals:
            vals[y_sqr].append(y)
        else:
            vals[y_sqr] = [y]
    return vals 

def get_filtered_pairs():    
    #Generate a solution space
    pairs = [Point(x,f(x)%p) for x in range(x_min,x_max)]
    #Filter in pairs that actually exist in the finite field
    filtered_pairs = []
    vals = get_squared_ys()
    for i in pairs: 
        if i.y in vals:
            roots = vals[i.y]
            for j in roots:
                filtered_pairs.append(Point(i.x, j))
    return filtered_pairs

def get_concated_solution(filtered_pairs):
    concat_solutions = {} 
    for i in filtered_pairs:
        if i.x in concat_solutions:
            concat_solutions[i.x].append(i.y)
        else:
            concat_solutions[i.x]=[i.y]
    return concat_solutions

def print_solutions(concated_solutions):
    print("x\t y")
    for key,val in zip(concated_solutions.keys(), concated_solutions.values()):
        print(str(key) +"\t"+ str(val))
        
def print_cycles(cycles):
    print("\nCycle \tPoints")
    for key,val in zip(cycles.keys(), cycles.values()):
        print(str(key) +"\t\t"+ str(val) )

def plot_solutions(filtered_pairs):    
    xs = [i.x for i in filtered_pairs]
    ys = [i.y for i in filtered_pairs]
    neg_ys = [-y for y in ys]
    fig = plt.gcf()
    plt.scatter(xs, ys)
    plt.scatter(xs, neg_ys)
    plt.xticks(range(x_min,x_max, 1), fontsize=7, rotation = 45)
    plt.yticks(range(min(neg_ys), max(ys), 1), fontsize = 7)
    
    plt.title("Solutions of the elliptic curve $y^2\equiv-x^3+"+str(A)+"x+"+str(B)+"$ over $\mathbb{Z}_{"+str(p)+"}$")
    plt.show()

def get_all_cycles(filtered_pairs):
    cycles={}
    for point in filtered_pairs:
        cycle=point.get_cycle()
        if cycle in cycles:
            cycles[cycle].append(point)
        else:
            cycles[cycle]=[point]
    return cycles

def monad():
    filtered_pairs = get_filtered_pairs()
    plot_solutions(filtered_pairs)
    
    concated_solutions = get_concated_solution(filtered_pairs)
    print_solutions(concated_solutions)
    
    cycles = get_all_cycles(filtered_pairs)
    print_cycles(cycles)
    return
            
def is_solution(i):
        return not bool(f(i.x)-i.y**2)

if __name__ == '__main__': 
    monad()

def test_add_infty():
    a = Point(10,-18)
    b = Point(10,-1)
    c = a+b
    assert np.isnan(c.x)
    assert np.isnan(c.y)
        
def test_add_self():
    a = Point(6,5)
    c = a+a
    assert Point(8,12)==c
        
def test_add():
    a = Point(0,6)
    b = Point(6,5)
    c = a+b
    assert Point(3,4)==c